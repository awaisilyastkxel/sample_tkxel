Getting Started
---------------


 1. `bundle install command` 
 2. `bundle exec rake db:create`
 3. `bundle exec rake db:migrate`

----------


Description
-----------

This is Simple but interesting API using which you can, create, update and delete user. User authentication has also been added using devise. User can create, update, delete, get his specific post and can get his all of the posts. 

Suggestion:

Instead of going through the code in controller. Read all the specs. Specs are located at following directory.

spec/api/v1/

----------

API Routes
----------
**User**

	POST   /api/users/sign_in LOGIN
	POST   /api/users         Signup
	PUT    /api/users         Update
	DELETE /api/users         Delete

**Post**

      GET    /api/users/:user_id/posts       INDEX
      POST   /api/users/:user_id/posts       CREATE
      GET    /api/users/:user_id/posts/:id   SHOW
      PUT    /api/users/:user_id/posts/:id   Update
      DELETE /api/users/:user_id/posts/:id
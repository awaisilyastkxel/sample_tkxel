SampleTkxel::Application.routes.draw do
  namespace :api do
    devise_for :users,  :controllers => { sessions: "api/sessions", registrations: "api/registrations" }, :module => "api"
  	resources :users do
  	  resources :posts
  	end
  end

  match 'api/users/:id' => 'api/registrations#destroy', via: :delete
end
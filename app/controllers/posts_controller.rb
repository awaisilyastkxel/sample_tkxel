class Api::PostsController < ApplicationController
  respond_to :json
  before_filter :authenticate_api_user!

  def index
    user = User.find(params[:user_id])
    return render json: 'user not found', status: 404 unless user

    if user == current_api_user
      posts = user.posts 
      return render json: posts, status: 201
    else
      return render json: 'You can only access your data', status: 400
    end
  end

  def show
    user = User.find_by_id(params[:user_id])
    return render json: 'user not found', status: 404 unless user
    post = user.posts.find_by_id(params[:id])

    if post.present?
      render json: post, status: 201
    else
      render json: 'post not found', status: 404
    end
  end

  def create
    user = User.find_by_id(params[:user_id])
    return render json: 'user not found', status: 404 unless user
    post = user.posts.create(params[:post])
    
    if post
      render json: post, status: 200
    else
      render json: post.errors, status: 404
    end
  end

  def update
    user = User.find_by_id(params[:user_id])
    return render json: 'user not found', status: 404 unless user
    post = user.posts.find_by_id(params[:id])

    if post && post.update_attributes(params[:post])
      render json: post, status: 201
    else
      render json: 'post not found', status: 404
    end
  end

  def destroy
    user = User.find_by_id(params[:user_id])
    render json: 'user not found', status: 404 unless user
    post = user.posts.find_by_id(params[:id])

    if post.destroy
      render json: 'deleted successfully', status: 201
    else
      render json: 'post not found', status: 404
    end
  end
end

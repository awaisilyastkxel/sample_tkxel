class Api::SessionsController < Devise::SessionsController

  def create
    user = User.find_for_database_authentication(:email => JSON.parse(params[:user])["email"])

    if user && user.valid_password?(JSON.parse(params[:user])["password"])
      sign_in user
      render :json => { :message => "user logged in successfully" }, :status => 200
    else
      return invalid_login_attempt
    end
  end

  def destroy
    user = User.find(params[:authentication_token])    
    return render json: { errors: I18n::t('user_not_found') }, status: 400 unless user

    user.reset_authentication_token!
    render json: { message: ["Session deleted."] }, success: true, status: 200
  end

  private

  def invalid_login_attempt
    warden.custom_failure!
    render :json => { :errors => ["Invalid email or password."] },  :success => false, :status => 400
  end
end
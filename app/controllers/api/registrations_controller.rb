class Api::RegistrationsController < Devise::RegistrationsController
  respond_to :json

  def create 
    params_data = parse_json_request
    return render json: { errors: I18n::t('invalid_format') }, status: 400 unless params_data

    user = User.new params_data

    if user.save
      return render json: user.as_json(auth_token: user.authentication_token, email: user.email), status: 201
    else
      warden.custom_failure!
      render json: user.errors, status: 422
    end
  end

  def update
    if session["warden.user.api_user.key"].first.present?
      user = User.find(session["warden.user.api_user.key"][0][0])
      if user.update_attributes(JSON.parse(params[:user]))
        return render json: user.as_json(name: user.name, email: user.email), status: 201
      end
    else
      return render json: { errors: I18n::t('user_not_logged_in') }, status: 401
    end
  end

  private

  def parse_json_request
    JSON.parse(params[:user])
  rescue
    false
  end
end
class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :status
      t.belongs_to :user
      
      t.timestamps
    end
  end
end

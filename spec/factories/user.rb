FactoryGirl.define do
  factory :user do
    sequence(:email) {|n| "test#{n}@foobar.com" }
    name 'FooBar'
    password 'testing123'
    password_confirmation 'testing123'
  end
end

require 'spec_helper'

describe 'User Api' do
  describe "POST create" do
    context "with valid attributes" do
      it "should create the user" do
        post api_user_registration_path, user: FactoryGirl.attributes_for(:user).to_json
        user_id = JSON.load(response.body)['id']
        response.status.should == 201
      end    
    end

    context "with invalid attributes" do
      it "should not create the user" do
        User.any_instance.stub(:save).and_return(false)
        post api_user_registration_path, user: FactoryGirl.attributes_for(:user).to_json
        response.status.should == 422
      end
    end

    context "with invalid format" do
      it "should give error message" do
        post api_user_registration_path, user: FactoryGirl.attributes_for(:user)
        response.status.should == 400
        JSON.load(response.body)['errors'].should eql I18n::t('invalid_format')
      end    
    end
  end

  describe "PUT update" do
    context "when the user is not logged in" do
      it "should not update the user" do
        user = FactoryGirl.create(:user)
        put api_user_registration_path(user), user: { name: 'Awais' }.to_json
        response.status.should == 401
      end
    end

    context "with valid attributes" do
      it "should allow to update user" do
        user = FactoryGirl.create(:user)
        post '/api/users/sign_in', 'user[email]' => user.email, 'user[password]' => user.password
        response.status.should == 200
        put api_user_registration_path(user), user: { name: 'Awais' }.to_json
        response.status.should == 201
      end
    end
  end

  describe "Login User" do
    context "with valid attributes" do
      it "should logged in the user" do
        post api_user_session_path, user: FactoryGirl.create(:user).to_json
        response.status.should == 200
      end    
    end

    context "with invalid attributes" do
      it "should not logged in the user" do
        User.any_instance.stub(:valid_password?).and_return(false)
        post api_user_session_path, user: FactoryGirl.create(:user).to_json
        response.status.should == 400
      end
    end

    context "with invalid format" do
      it "should give error message" do
        post api_user_registration_path, user: FactoryGirl.attributes_for(:user)
        response.status.should == 400
        JSON.load(response.body)['errors'].should eql I18n::t('invalid_format')
      end    
    end
  end
end

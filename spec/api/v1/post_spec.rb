require 'spec_helper'

describe 'Post Api' do
  let(:user) { FactoryGirl.create :user }
  let(:second_user) { FactoryGirl.create :user }
  let(:user_post) { FactoryGirl.create :post, user: user }
  
  describe "POST INDEX" do
    context "when the user is not logged in" do
      it "should show error message" do
        get api_user_posts_path(user)
        response.status.should == 302 # unauthorized user redirected for signin
      end    
    end

    context "when user try to access some other users post" do
      it "should show error message" do
        User.any_instance.stub(:valid_password?).and_return(true)
        post api_user_session_path, user: user.to_json
        get api_user_posts_path(second_user)
        response.status.should == 400
      end    
    end


    context "when the user is logged in" do
      it "should show all the users posts" do
        User.any_instance.stub(:valid_password?).and_return(true)
        post api_user_session_path, user: user.to_json
        user_post
        get api_user_posts_path(user)
        response.status.should == 201
      end    
    end
  end

  describe "GET SHOW" do
    context "when the user is not logged in" do
      it "should throw and exception" do
        get api_user_posts_path(user)
        response.status.should == 302 # unauthorized user redirected for signin
      end    
    end

    context "when user is logged in" do
      it "should show requested post" do
        User.any_instance.stub(:valid_password?).and_return(true)
        post api_user_session_path, user: user.to_json
        user_post
        get api_user_post_path(user, user_post)
        response.status.should == 201
      end    
    end
  end

  describe "POST CREATE" do
    context "when user is logged in" do
      it "should create the post" do
        User.any_instance.stub(:valid_password?).and_return(true)
        post api_user_session_path, user: user.to_json
        post api_user_posts_path(user, post: FactoryGirl.attributes_for(:post, user: user))
        user.posts.count.should == 1
        response.status.should == 200
      end    
    end
  end

  describe "PUT UPDATE" do
    context "when user is logged in" do
      it "should allow the user to update the post" do
        User.any_instance.stub(:valid_password?).and_return(true)
        post api_user_session_path, user: user.to_json
        put api_user_post_path(user, user_post), post: { status: 'Updated the status' }
        response.status.should == 201
      end    
    end
  end
end
